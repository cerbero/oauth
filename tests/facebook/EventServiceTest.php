<?php

use Mockery as m;

/**
 * Tests about Facebook events.
 *
 * @author	Andrea Marco Sartori
 */
class EventServiceTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$this->client = m::mock('Cerbero\Oauth\Providers\Clients\Facebook');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * Bind the event service.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function bindEvent()
	{
		$this->bindService('event', 'facebook');
	}

	/**
	 * @testdox	Callable by facade.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCallableByFacade()
	{
		$event = Facebook::event();

		$service = 'Cerbero\Oauth\Providers\Services\Facebook\Event';

		$this->assertInstanceOf($service, $event);
	}

	/**
	 * @testdox	Retrieve an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAnEvent()
	{
		$this->client->shouldReceive('api')->once()->with(5, 'GET', array());

		$this->bindEvent();

		Facebook::event(5)->get();
	}

	/**
	 * @testdox	Retrieve user events.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveUserEvents()
	{
		$this->client->shouldReceive('api')->once()->with('me/events', 'GET', array());

		$this->bindService('event', 'facebook', null);

		Facebook::event()->get();
	}

	/**
	 * @testdox	Delete an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testDeleteAnEvent()
	{
		$this->client->shouldReceive('api')->once()->with(5, 'DELETE', array());

		$this->bindEvent();

		Facebook::event(5)->remove();
	}

	/**
	 * @testdox	Update an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testUpdateAnEvent()
	{
		$params = array('name' => 'name', 'message' => 'description');

		$this->client->shouldReceive('api')->once()->with('5', 'POST', $params)->andReturn(true);

		$this->bindEvent();

		$boolean = Facebook::event(5)->update($params);

		$this->assertTrue($boolean);
	}

	/**
	 * @testdox	Create an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCreateAnEvent()
	{
		$params = array('name' => 'name', 'message' => 'description');

		$this->client->shouldReceive('api')->once()->with('me/events', 'POST', $params)->andReturn(array('id' => 1));

		$this->bindService('event', 'facebook', null);

		$id = Facebook::event()->create($params);

		$this->assertSame(1, $id);
	}

	/**
	 * @testdox	Retrieve attenders.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAttenders()
	{
		$this->client->shouldReceive('api')->once()->with('5/attending', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->attenders();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Attend an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAttendAnEvent()
	{
		$this->client->shouldReceive('api')->once()->with('5/attending', 'POST', array())->andReturn(true);

		$this->bindEvent();

		$boolean = Facebook::event(5)->attend();

		$this->assertTrue($boolean);
	}

	/**
	 * @testdox	Retrieve decliners.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveDecliners()
	{
		$this->client->shouldReceive('api')->once()->with('5/declined', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->decliners();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Decline an event.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testDeclineAnEvent()
	{
		$this->client->shouldReceive('api')->once()->with('5/declined', 'POST', array())->andReturn(true);

		$this->bindEvent();

		$boolean = Facebook::event(5)->decline();

		$this->assertTrue($boolean);
	}

	/**
	 * @testdox	Retrieve invited.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveInvited()
	{
		$this->client->shouldReceive('api')->once()->with('5/invited', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->invited();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Invite people.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testInvitePeople()
	{
		$this->client->shouldReceive('api')->twice()->with('5/invited', 'POST', array('users' => '1,2,3'))->andReturn(true);

		$this->bindEvent();

		$boolean = Facebook::event(5)->invite(1, 2, 3);

		$this->assertTrue($boolean);

		$boolean = Facebook::event(5)->invite(array(1, 2, 3));

		$this->assertTrue($boolean);
	}

	/**
	 * @testdox	Exclude people.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testExcludePeople()
	{
		$this->client->shouldReceive('api')->twice()->with('5/invited', 'DELETE', array('user' => 1))->andReturn(true);

		$this->client->shouldReceive('api')->twice()->with('5/invited', 'DELETE', array('user' => 2))->andReturn(true);

		$this->client->shouldReceive('api')->twice()->with('5/invited', 'DELETE', array('user' => 3))->andReturn(true);

		$this->bindEvent();

		$boolean = Facebook::event(5)->exclude(1, 2, 3);

		$this->assertTrue($boolean);

		$boolean = Facebook::event(5)->exclude(array(1, 2, 3));

		$this->assertTrue($boolean);
	}

	/**
	 * @testdox	Retrieve all posts.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllPosts()
	{
		$this->client->shouldReceive('api')->once()->with('5/feed', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->posts();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Add a post to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAPostToTheWall()
	{
		$params = array('message' => 'text');

		$this->client->shouldReceive('api')->once()->with('5/feed', 'POST', $params)->andReturn(array('id' => 2));

		$this->bindEvent();

		$id = Facebook::event(5)->post($params);

		$this->assertSame(2, $id);
	}

	/**
	 * @testdox	Retrieve people who replied with maybe.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrievePeopleWhoRepliedWithMaybe()
	{
		$this->client->shouldReceive('api')->once()->with('5/maybe', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->maybes();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Reply with maybe.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testReplyWithMaybe()
	{
		$this->client->shouldReceive('api')->once()->with('5/maybe', 'POST', array())->andReturn(true);

		$this->bindEvent();

		$boolean = Facebook::event(5)->maybe();

		$this->assertTrue($boolean);
	}

	/**
	 * @testdox	Retrieve people who haven't replied yet.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrievePeopleWhoHaventRepliedYet()
	{
		$this->client->shouldReceive('api')->once()->with('5/noreply', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->noreplies();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Retrieve the picture.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveThePicture()
	{
		$params = array('type' => 'normal', 'redirect' => false);

		$this->client->shouldReceive('api')->once()->with('5/picture', 'GET', $params)->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->picture('normal', false);

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Set the picture by source.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSetThePictureBySource()
	{
		$uploadedFile = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$uploadedFile->shouldReceive('getRealPath')->once();

		$uploadedFile->shouldReceive('getMimeType')->once();

		$this->client->shouldReceive('api')->with('5/picture', 'POST', m::on(function($params)
		{
			$this->assertInstanceOf('CURLFile', $params['source']);

			return true;

		}))->andReturn(m::self());

		$this->bindEvent();

		$return = Facebook::event(5)->setPicture($uploadedFile);

		$this->assertInstanceOf('Cerbero\Oauth\Providers\Services\Facebook\Event', $return);
	}

	/**
	 * @testdox	Set the picture by URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSetThePictureByUrl()
	{
		$params = array('url' => 'https://www.google.com');

		$this->client->shouldReceive('api')->once()->with('5/picture', 'POST', $params)->andReturn(m::self());

		$this->bindEvent();

		$return = Facebook::event(5)->setPicture('https://www.google.com');

		$this->assertInstanceOf('Cerbero\Oauth\Providers\Services\Facebook\Event', $return);
	}

	/**
	 * @testdox	Retrieve all photos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllPhotos()
	{
		$this->client->shouldReceive('api')->once()->with('5/photos', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->photos();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Add a photo.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAPhoto()
	{
		$params = array('url' => 'https://www.google.com', 'message' => '');

		$this->client->shouldReceive('api')->once()->with('5/photos', 'POST', $params)->andReturn(array('id' => 3));

		$this->bindEvent();

		$id = Facebook::event(5)->addPhoto('https://www.google.com');

		$this->assertSame(3, $id);
	}

	/**
	 * @testdox	Add a photo during event creation.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAPhotoDuringEventCreation()
	{
		$uploadedFile = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$uploadedFile->shouldReceive('getMimeType')->once();

		$uploadedFile->shouldReceive('getRealPath')->once();

		$uploadedFile->shouldReceive('guessExtension')->once()->andReturn('png');

		$params1 = array('name' => 'event', 'message' => 'eventDesc');

		$this->client->shouldReceive('api')->once()->with('me/events', 'POST', $params1)->andReturn(array('id' => 12));

		$this->client->shouldReceive('api')->once()->with('12/photos', 'POST', m::on(function($params2)
		{
			$this->assertInstanceOf('CURLFile', $params2['source']);

			$this->assertSame('photoDesc', $params2['message']);

			return true;

		}))->andReturn(array('id' => 15));

		$this->bindService('event', 'facebook', null);

		$id = Facebook::event()->with($uploadedFile, 'photoDesc')->create($params1);

		$this->assertSame(12, $id);
	}

	/**
	 * @testdox	Retrieve all videos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllVideos()
	{
		$this->client->shouldReceive('api')->once()->with('5/videos', 'GET', array())->andReturn(array('foo'));

		$this->bindEvent();

		$array = Facebook::event(5)->videos();

		$this->assertInternalType('array', $array);
	}

	/**
	 * @testdox	Add a video.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAVideo()
	{
		$uploadedFile = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$uploadedFile->shouldReceive('getMimeType')->once();

		$uploadedFile->shouldReceive('getRealPath')->once();

		$this->client->shouldReceive('api')->with('5/videos', 'POST', m::on(function($params)
		{
			$this->assertInstanceOf('CURLFile', $params['source']);

			$this->assertSame('title', $params['title']);

			$this->assertSame('desc', $params['description']);

			return true;

		}))->andReturn(array('id' => 22));

		$this->bindEvent();

		$id = Facebook::event(5)->addVideo($uploadedFile, 'title', 'desc');

		$this->assertSame(22, $id);
	}

	/**
	 * @testdox	Add a video during event creation.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAVideoDuringEventCreation()
	{
		$uploadedFile = m::mock('Symfony\Component\HttpFoundation\File\UploadedFile');

		$uploadedFile->shouldReceive('getMimeType')->once();

		$uploadedFile->shouldReceive('getRealPath')->once();

		$uploadedFile->shouldReceive('guessExtension')->andReturn('avi');

		$params1 = array('name' => 'event', 'message' => 'eventDesc');

		$this->client->shouldReceive('api')->once()->with('me/events', 'POST', $params1)->andReturn(array('id' => 12));

		$this->client->shouldReceive('api')->once()->with('12/videos', 'POST', m::on(function($params2)
		{
			$this->assertInstanceOf('CURLFile', $params2['source']);

			$this->assertSame('videoTitle', $params2['title']);

			$this->assertSame('videoDesc', $params2['description']);

			return true;

		}))->andReturn(array('id' => 15));

		$this->bindService('event', 'facebook', null);

		$id = Facebook::event()->with($uploadedFile, 'videoTitle', 'videoDesc')->create($params1);

		$this->assertSame(12, $id);
	}

}