<?php

use Mockery as m,
	Cerbero\Oauth\Providers\Facebook;

/**
 * Tests about the Facebook provider.
 *
 * @author	Andrea Marco Sartori
 */
class FacebookTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$config = array('appId' => '', 'secret' => '');
		
		$this->client = m::mock('Cerbero\Oauth\Providers\Clients\Facebook');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * @testdox	Generate authentication URL with scope.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testGenerateAuthenticationURLWithScope()
	{
		$param = array('scope' => array('testScope'), 'redirect_uri' => url("login/oauth/facebook"));

		$client = $this->client->shouldReceive('getLoginUrl')->once()->with($param)->mock();

		$fb = new Facebook($client);

		$fb->getAuthUrl(array('testScope'));
	}

	/**
	 * @testdox	Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testStoreTheAccessToken()
	{
		$client = $this->client->shouldReceive('getAccessToken')->once()->mock();

		$fb = new Facebook($client);

		$fb->storeAccessToken();
	}

	/**
	 * @testdox	Retrieve the used client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUsedClient()
	{
		$fb = new Facebook($this->client);

		$client = $fb->getClient();

		$this->assertInstanceOf('\BaseFacebook', $client);
	}

	/**
	 * @testdox	Retrieve the user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserIdentifier()
	{
		$client = $this->client->shouldReceive('getUser')->once()->mock();

		$fb = new Facebook($client);

		$fb->getID();
	}

	/**
	 * @testdox	Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserEmail()
	{
		$return = array('email' => 'test@example.com');

		$client = $this->client->shouldReceive('api')->once()->with('/me')->andReturn($return)->mock();

		$fb = new Facebook($client);

		$email = $fb->getEmail();

		$this->assertSame('test@example.com', $email);
	}

	/**
	 * @testdox	Exception thrown if the email is not provided.
	 * @expectedException UnexpectedValueException
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testExceptionThrownIfTheEmailIsNotProvided()
	{
		$client = $this->client->shouldReceive('api')->once()->with('/me')->mock();

		$fb = new Facebook($client);

		$fb->getEmail();
	}

}