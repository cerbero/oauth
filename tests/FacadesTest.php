<?php

/**
 * Tests about provider facades.
 *
 * @author	Andrea Marco Sartori
 */
class FacadesTest extends TestCase
{

	/**
	 * @testdox	Google facade works.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testGoogleFacadeWorks()
	{
		$client = Google::client();

		$this->assertInstanceOf('Google_Client', $client);
	}

	/**
	 * @testdox	Facebook facade works.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testFacebookFacadeWorks()
	{
		$client = Facebook::client();

		$this->assertInstanceOf('BaseFacebook', $client);
	}

	/**
	 * @testdox	Twitter facade works.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testTwitterFacadeWorks()
	{
		$client = Twitter::client();

		$this->assertInstanceOf('TwitterOAuth', $client);
	}

	/**
	 * @testdox	Yahoo facade works.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testYahooFacadeWorks()
	{
		$client = Yahoo::client();

		$this->assertInstanceOf('YahooOAuthApplication', $client);
	}

}