<?php

use Mockery as m,
	Cerbero\Oauth\Providers\Yahoo;

/**
 * Tests about Yahoo provider.
 *
 * @author	Andrea Marco Sartori
 */
class ProviderTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();
		
		$this->client = m::mock('YahooOAuthApplication', array(1, 2, 3));

		$this->storage = m::mock('Cerbero\Oauth\Storage\TokenStorageInterface')->shouldReceive('get')->with('yahoo.access')->mock();
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * @testdox	Retrieve the authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheAuthorizationURL()
	{
		$callback = url('login/oauth/yahoo');

		$requestToken = m::mock('TokenMock');

		$this->client->shouldReceive('getRequestToken')->with($callback)->andReturn($requestToken);

		$this->client->shouldReceive('getAuthorizationUrl')->once()->with($requestToken)->andReturn('yahooUrl');

		$this->storage->shouldReceive('get')->twice()->with('yahoo.request')->andReturn($requestToken);

		$this->storage->shouldReceive('set')->with('yahoo.request', $requestToken);

		$yahoo = new Yahoo($this->client, $this->storage);

		$url = $yahoo->getAuthUrl();

		$this->assertSame('yahooUrl', $url);
	}

	/**
	 * @testdox	Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testStoreTheAccessToken()
	{
		Input::replace(array('oauth_verifier' => 1234));

		$requestToken = m::mock('TokenMock');

		$this->client->shouldReceive('getRequestToken')->andReturn($requestToken);

		$this->client->shouldReceive('getAccessToken')->once()->with($requestToken, 1234)->andReturn('accessToken');

		$this->storage->shouldReceive('get')->with('yahoo.request')->andReturn($requestToken);

		$this->storage->shouldReceive('set')->once()->with('yahoo.access', 'accessToken');

		$this->storage->shouldReceive('remove')->once()->with('yahoo.request');

		$yahoo = new Yahoo($this->client, $this->storage);

		$yahoo->storeAccessToken();
	}

	/**
	 * @testdox	Retrieve the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheClient()
	{
		$yahoo = new Yahoo($this->client, $this->storage);

		$client = $yahoo->getClient();

		$this->assertInstanceOf('YahooOAuthApplication', $client);
	}

	/**
	 * @testdox	Retrieve the user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserIdentifier()
	{
		$this->client->shouldReceive('getGUID')->once()->andReturn(1234);

		$yahoo = new Yahoo($this->client, $this->storage);

		$id = $yahoo->getID();

		$this->assertSame(1234, $id);
	}

	/**
	 * @testdox	Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUserEmail()
	{
		$email = m::mock('EmailMock');

		$email->handle = 'test@example.com';

		$email->primary = true;

		$profileProp = m::mock('ProfilePropMock');

		$profileProp->emails = array($email);

		$profile = m::mock('ProfileMock');

		$profile->profile = $profileProp;

		$this->client->shouldReceive('getProfile')->once()->andReturn($profile);

		$yahoo = new Yahoo($this->client, $this->storage);

		$address = $yahoo->getEmail();

		$this->assertSame('test@example.com', $address);
	}

}