<?php namespace Cerbero\Oauth\Providers\Services;

/**
 * Service abstraction.
 *
 * @author	Andrea Marco Sartori
 */
abstract class AbstractService implements ServiceInterface
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$attributes	Service attributes.
	 */
	protected $attributes;

	/**
	 * Set the service attributes.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$attributes
	 * @return	void
	 */
	public function setAttributes($attributes)
	{
		$this->attributes = $attributes;
	}

	/**
	 * Retrieve the attributes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function getAttributes()
	{
		$attr = $this->attributes;

		return count($attr) > 1 ? $attr : $attr[0];
	}

	/**
	 * Retrieve the provider client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	protected function client()
	{
		$provider = strtolower($this->getProvider());

		return app("oauth.{$provider}")->client();
	}

	/**
	 * Retrieve the provider name.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string
	 */
	protected function getProvider()
	{
		$segments = explode('\\', get_called_class());

		$penultimate = count($segments) - 2;

		return $segments[$penultimate];
	}

}