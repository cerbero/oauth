<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

/**
 * Service for comments.
 *
 * @author	Andrea Marco Sartori
 */
class Comment extends AbstractFacebookService
{

	/**
	 * Retrieve a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function get()
	{
		return parent::get();
	}

	/**
	 * Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function likes()
	{
		return parent::likes();
	}

	/**
	 * Like a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	public function like()
	{
		return parent::like();
	}

	/**
	 * Dislike a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	public function dislike()
	{
		return parent::dislike();
	}

	/**
	 * Remove a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @return	int
	 */
	public function remove()
	{
		return parent::remove();
	}

}