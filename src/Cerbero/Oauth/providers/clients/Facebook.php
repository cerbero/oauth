<?php namespace Cerbero\Oauth\Providers\Clients;

use Cerbero\Oauth\Storage\TokenStorageInterface;

/**
 * Client for Facebook SDK.
 *
 * @author	Andrea Marco Sartori
 */
class Facebook extends \BaseFacebook
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		Cerbero\Oauth\Storage\TokenStorageInterface	$storage	Storage for tokens.
	 */
	protected $storage;

	/**
	 * Set the token storage.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	Cerbero\Oauth\Storage\TokenStorageInterface	$storage
	 * @param	array	$config
	 * @return	void
	 */
	public function __construct(TokenStorageInterface $storage, array $config)
	{
		$this->storage = $storage;

		$this->configure($config);
	}

	/**
	 * Configure the facebook client.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$config
	 * @return	void
	 */
	protected function configure($config)
	{
		$this->setPersistentData('state', csrf_token());

		parent::__construct(array
		(
			'appId' => $config['application_id'],

			'secret' => $config['application_secret'],

			'fileUpload' => true
		));
	}

	/**
	 * Stores the given ($key, $value) pair, so that future calls to
	 * getPersistentData($key) return $value. This call may be in another request.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string $key
	 * @param	array $value
	 * @return	void
	 */
	protected function setPersistentData($key, $value)
	{
		$this->storage->set("facebook.{$key}", $value);
	}

	/**
	 * Get the data for $key, persisted by BaseFacebook::setPersistentData()
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string $key The key of the data to retrieve
	 * @param	boolean $default The default value to return if $key is not found
	 * @return	mixed
	 */
	protected function getPersistentData($key, $default = false)
	{
		return $this->storage->get("facebook.{$key}", $default);
	}

	/**
	 * Clear the data with $key from the persistent storage
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string $key
	 * @return	void
	 */
	protected function clearPersistentData($key)
	{
		$this->storage->remove("facebook.{$key}");
	}

	/**
	 * Clear all data from the persistent storage
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function clearAllPersistentData()
	{
		$this->storage->remove('facebook');
	}

}