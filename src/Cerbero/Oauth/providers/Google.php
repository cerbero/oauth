<?php namespace Cerbero\Oauth\Providers;

use Cerbero\Oauth\Storage\TokenStorageInterface,
	\Google_Client,
	\Input;

/**
 * Google provider.
 *
 * @author	Andrea Marco Sartori
 */
class Google extends AbstractProvider
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		Google_Client	$client	The google client.
	 */
	protected $client;

	/**
	 * @author	Andrea Marco Sartori
	 * @var		Cerbero\Oauth\Storage\TokenStorageInterface	$storage	The storage used to save tokens.
	 */
	protected $storage;

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$tokenAttributes	Attributes of the ID token.
	 */
	protected $tokenAttributes;

	/**
	 * Initialise the client and configure it.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	Google_Client	$client
	 * @param	TokenStorageInterface	$client
	 * @param	array	$config
	 * @return	void
	 */
	public function __construct(Google_Client $client, TokenStorageInterface $storage,  array $config)
	{
		$this->client = $client;

		$this->storage = $storage;

		$this->configure($config);
	}

	/**
	 * Configure the provider client.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$config
	 * @return	void
	 */
	protected function configure($config)
	{
		$this->client->setClientId( $config['client_id'] );

		$this->client->setClientSecret( $config['client_secret'] );

		$this->client->setRedirectUri( $this->getRedirectUri() );

		$this->client->setState( csrf_token() );
	}

	/**
	 * Retrieve the authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array|null	$scopes
	 * @return	string
	 */
	public function getAuthUrl($scopes = null)
	{
		$this->client->setScopes($scopes);

		return $this->client->createAuthUrl();
	}

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function storeAccessToken()
	{
		$code = Input::get('code');

		$token = $this->client->authenticate($code);

		$this->storage->set('google', $token);
	}

	/**
	 * Set the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function setAccessToken()
	{
		$accessToken = $this->storage->get('google');

		$this->client->setAccessToken($accessToken);
	}

	/**
	 * Return the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	Google_Client
	 */
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * Retrieve the unique user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getID()
	{
		$attributes = $this->getTokenAttributes();

		return $attributes['payload']['sub'];
	}

	/**
	 * Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	string|UnexpectedValueException
	 */
	public function getEmail()
	{
		$attributes = $this->getTokenAttributes();

		if( ! isset($attributes['payload']['email']))
		{
			throw new \UnexpectedValueException('Email is not provided by the given scopes.');
		}
		return $attributes['payload']['email'];
	}

	/**
	 * Retrieve the token attributes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function getTokenAttributes()
	{
		$this->setAccessToken();

		if($attributes = $this->tokenAttributes) return $attributes;

		return $this->tokenAttributes = $this->client->verifyIdToken()->getAttributes();
	}

}