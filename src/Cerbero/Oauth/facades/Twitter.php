<?php namespace Cerbero\Oauth\Facades;

use \Illuminate\Support\Facades\Facade;

/**
 * @see \Cerbero\Oauth\Providers\Twitter
 */
class Twitter extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'oauth.twitter'; }

}